<?php

namespace app\controllers;

use app\models\Enroll;
use app\models\EnrollProgress;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use app\models\forms\ContactForm;
use app\models\forms\RegisterForm;
use yii\widgets\ActiveForm;

class DashboardController extends Controller
{

    public $layout = 'dashboard';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $enrolls = Enroll::find()
                    ->where(['enroll_userid' => Yii::$app->user->identity->user_id])
                    ->Andwhere(['enroll_is_deleted' => 0])
                    ->all();
        foreach($enrolls as $key => $value)
        {
            $progress[] = EnrollProgress::findAll(['ep_enroll_id' => $value->enroll_id, 'ep_remark' => "quiz"]);   
        }

        return $this->render('index', [
            'enrolls' => $enrolls,
            'progress' => $progress,
        ]);
    }
}

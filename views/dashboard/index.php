<?php

use yii\helpers\Url;

$this->title = "Dashboard";
?>

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Hai <?= Yii::$app->user->identity->user_nama ?></h3>
    </div>

    <div class="card-body">
        <p>Welcome to DIGITAL TVET ACADEMY</p>

        <p>You are now enrolls to take our courses and all of our paid courses. Enjoy your course!</p>

        <p>Click <a href="<?= Url::to(['site/courses']) ?>">here</a> to see more courses</p>
    </div>

</div>

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Your Enrollment Courses</h3>
    </div>

    <div class="card-body">
        <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Category</th>
                    <th>Task</th>
                    <th>Progress</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1;
                foreach ($enrolls as $key => &$enroll) : ?>
                    <tr>
                        <td><?= $no ?></td>
                        <td><?= $enroll->enrollCourse->course_type ?></td>
                        <td><?= $enroll->enrollCourse->course_title ?>
                            <?php
                            foreach($progress[$key] as $key => &$value)
                            {
                                $nilai = ($value["ep_nilai"] != null)?$value["ep_nilai"] : 0 ;
                                echo "<br>Assessment ".($key+1)." ( Mark : ".$nilai.")";
                            }
                            ?>
                        </td>
                        <td>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: <?=($enroll->enrollProgress)?>%" aria-valuenow="<?=($enroll->enrollProgress)?>" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            (<?=($enroll->enrollProgressText)?> Section ) <?=($enroll->enrollProgress)?>%
                        </td>
                        <td><?= date("Y-m-d", strtotime($enroll->enroll_created_at)); ?></td>
                        <td><a href="<?= Url::to(['site/enroll', 'id' => $enroll->enroll_id]) ?>" class="btn btn-primary">Start Now</a></td>
                    </tr>
                <?php $no++;
                endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
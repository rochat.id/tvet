<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Enroll $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="enroll-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [

            'enroll_userid' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Enroll Userid...']],

            'enroll_courseid' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Enroll Courseid...']],

            'enroll_cs' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Enroll Cs...']],

            'enroll_status' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Enroll Status...']],

            'enroll_remark' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Enroll Remark...', 'maxlength' => 100]],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Enroll $model
 */

$this->title = 'Update Enroll';
$this->params['breadcrumbs'][] = ['label' => 'Enrolls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->enroll_id, 'url' => ['view', 'id' => $model->enroll_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="enroll-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

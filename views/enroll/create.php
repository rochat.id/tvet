<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Enroll $model
 */

$this->title = 'Create Enroll';
$this->params['breadcrumbs'][] = ['label' => 'Enrolls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="enroll-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

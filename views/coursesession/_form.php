<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\CourseSession $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="course-session-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [

            'cs_course_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Cs Course ID...']],

            'cs_teacher_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Cs Teacher ID...']],

            'cs_date_start' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => DateControl::classname(),'options' => ['type' => DateControl::FORMAT_DATETIME]],

            'cs_date_end' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => DateControl::classname(),'options' => ['type' => DateControl::FORMAT_DATETIME]],

            'cs_hour_start' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => DateControl::classname(),'options' => ['type' => DateControl::FORMAT_DATETIME]],

            'cs_hour_end' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => DateControl::classname(),'options' => ['type' => DateControl::FORMAT_DATETIME]],

            'cs_dateline' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => DateControl::classname(),'options' => ['type' => DateControl::FORMAT_DATETIME]],

            'cs_desc' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Cs Desc...','rows' => 6]],

            'cs_remark' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Cs Remark...', 'maxlength' => 100]],

            'cs_teacher' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Cs Teacher...', 'maxlength' => 100]],

            'cs_email' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Cs Email...', 'maxlength' => 50]],

            'cs_next_course' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Cs Next Course...', 'maxlength' => 50]],

            'cs_code' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Cs Code...', 'maxlength' => 50]],

            'cs_price' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Cs Price...', 'maxlength' => 25]],

            'cs_doc' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Cs Doc...', 'maxlength' => 255]],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>

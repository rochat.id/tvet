<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$baseUrl = Url::base() . '/academic';
$this->title = 'ROCHAT';

?>



<div class="hero-slide owl-carousel site-blocks-cover">

  <?php foreach ($banner_list as $key => $value) : ?>
    <div class="intro-section" style="background-image: url('<?= Url::base() ?>/<?= $value["b_foto"] ?>');">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-12 mx-auto text-center" data-aos="fade-up">
            <h1><a class="text-white" href="<?= $value["b_link"] ?>"><?= $value["b_title"] ?></a></h1>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
</div>
<!-- <iframe width="100%" height="480" src="https://www.youtube.com/embed/Mv_WXP4453s"></iframe> -->
<div></div>

<!-- <div class="site-section">
  <div class="container">
    <div class="row mb-8 justify-content-center text-center">
      <div class="col-lg-6 mb-5">
        <h2 class="section-title-underline mb-5">
          <span>Why Ro-Chat ?</span>
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">

        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-mortarboard text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Personalize Learning</h2>
            <p>Personalized learning is an educational approach that aims to customize learning for each student's strengths, needs, skills and interests.</p>
            <p><a href="<?= Url::to(['site/about']) ?>" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-school-material text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Trusted Courses</h2>
            <p>We choose the best lecturer and also the brand new courses that been tested and used on the industrial market</p>
            <p><a href="<?= Url::to(['site/about']) ?>" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-library text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Advanced Tools</h2>
            <p>We created the best tools to support courses for student and also for lecturer/trainer to make learning process easier.</p>
            <p><a href="<?= Url::to(['site/about']) ?>" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->


<div class="site-section">
  <div class="container">


    <div class="row mb-5 justify-content-center text-center">
      <div class="col-lg-6 mb-5">
        <h2 class="section-title-underline mb-3">
          <span>Our Products</span>
        </h2>
        <p>There are so many great product that you can choose</p>
      </div>
    </div>

    <div class="row">
      <div class="col-12">
        <div class="owl-slide-3 owl-carousel">
        <?php foreach ($course_list as $key => $value) : ?>
        <div class="course-1-item">
            <figure class="thumnail">
              <a href="<?= Url::to(['site/detailcourse']) ?>"><img src="<?= $baseUrl . $value["course_photo"] ?>" alt="Image" class="img-fluid"></a>
              <div class="price"><?= $value["course_price"] ?></div>
              <div class="category">
                <h3><?= $value["course_type"] ?></h3>
              </div>

              <!-- <div class=" center alert alert-primary">Online</div> -->
              <!-- <div class="center alert alert-info" role="alert">Online2</div> -->
            </figure>
            <div class="course-1-content pb-4">
              <h2><?= $value["course_title"].' - by <span class="badge badge-pill badge-success">'.$value["nama_trainer"]."</span>" ?> <?php if($value["course_is_online"]){echo '<span class="badge badge-pill badge-info">Online</span>';}else{echo '<span class="badge badge-pill badge-primary">Offline</span>';} ?></h2>

              <!-- <div class="rating text-center mb-3">
                <span class="icon-star2 text-warning"></span>
                <span class="icon-star2 text-warning"></span>
                <span class="icon-star2 text-warning"></span>
                <span class="icon-star2 text-warning"></span>
                <span class="icon-star2 text-warning"></span>
              </div> -->
              <!-- hide rating -->
              <p class="desc mb-4"><?= $value["course_desc"] ?></p>

              <p><a href="<?= Url::to(['site/detailcourse', 'id' => $value["course_id"]]) ?>" class="btn btn-primary rounded-0 px-4">Detail</a></p>
            </div>
          </div>
      <?php endforeach; ?>
          



        </div>

      </div>
    </div>



  </div>
</div>




<div class="section-bg style-1" style="background-image: url('<?= $baseUrl ?>/images/about_1.jpg');">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <h2 class="section-title-underline style-2">
          <span>About Us</span>
        </h2>
      </div>
      <div class="col-lg-8">
        <p class="lead">Ro-Chat </p>
        <p>is a platform for education online that connecting teachers with learners to provide contemporaneous and uncontemporaneous courses which can help them in coming across new things. There are various websites which are offering you the most comprehensive educators which can teach you by giving you a platform to choose your own way of learning and sometimes they also provide you the live instructions. Not only learning is done but you can check your performance by giving regular tests or by making assignments related to the topic which you have studied and if it is not possible for one to study with the live instructions then they can use self-paced instructions including hosting presentations, videos, screen sharing, break out of rooms, interactive whiteboards and much more.</p>
        <p><a href="#">Read more</a></p>
      </div>
    </div>
  </div>
</div>

<!-- // 05 - Block -->
<!-- <div class="site-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-lg-4">
            <h2 class="section-title-underline">
              <span>Testimonials</span>
            </h2>
          </div>
        </div>


        <div class="owl-slide owl-carousel">

          <div class="ftco-testimonial-1">
            <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
              <img src="<?= $baseUrl ?>/images/person_1.jpg" alt="Image" class="img-fluid mr-3">
              <div>
                <h3>Allison Holmes</h3>
                <span>Designer</span>
              </div>
            </div>
            <div>
              <p>&ldquo;Lorem ipsum dolor sit, amet consectetur adipisicing elit. Neque, mollitia. Possimus mollitia nobis libero quidem aut tempore dolore iure maiores, perferendis, provident numquam illum nisi amet necessitatibus. A, provident aperiam!&rdquo;</p>
            </div>
          </div>

          <div class="ftco-testimonial-1">
            <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
              <img src="<?= $baseUrl ?>/images/person_2.jpg" alt="Image" class="img-fluid mr-3">
              <div>
                <h3>Allison Holmes</h3>
                <span>Designer</span>
              </div>
            </div>
            <div>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Neque, mollitia. Possimus mollitia nobis libero quidem aut tempore dolore iure maiores, perferendis, provident numquam illum nisi amet necessitatibus. A, provident aperiam!</p>
            </div>
          </div>

          <div class="ftco-testimonial-1">
            <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
              <img src="<?= $baseUrl ?>/images/person_4.jpg" alt="Image" class="img-fluid mr-3">
              <div>
                <h3>Allison Holmes</h3>
                <span>Designer</span>
              </div>
            </div>
            <div>
              <p>&ldquo;Lorem ipsum dolor sit, amet consectetur adipisicing elit. Neque, mollitia. Possimus mollitia nobis libero quidem aut tempore dolore iure maiores, perferendis, provident numquam illum nisi amet necessitatibus. A, provident aperiam!&rdquo;</p>
            </div>
          </div>

          <div class="ftco-testimonial-1">
            <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
              <img src="<?= $baseUrl ?>/images/person_3.jpg" alt="Image" class="img-fluid mr-3">
              <div>
                <h3>Allison Holmes</h3>
                <span>Designer</span>
              </div>
            </div>
            <div>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Neque, mollitia. Possimus mollitia nobis libero quidem aut tempore dolore iure maiores, perferendis, provident numquam illum nisi amet necessitatibus. A, provident aperiam!</p>
            </div>
          </div>

          <div class="ftco-testimonial-1">
            <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
              <img src="<?= $baseUrl ?>/images/person_2.jpg" alt="Image" class="img-fluid mr-3">
              <div>
                <h3>Allison Holmes</h3>
                <span>Designer</span>
              </div>
            </div>
            <div>
              <p>&ldquo;Lorem ipsum dolor sit, amet consectetur adipisicing elit. Neque, mollitia. Possimus mollitia nobis libero quidem aut tempore dolore iure maiores, perferendis, provident numquam illum nisi amet necessitatibus. A, provident aperiam!&rdquo;</p>
            </div>
          </div>

          <div class="ftco-testimonial-1">
            <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
              <img src="<?= $baseUrl ?>/images/person_4.jpg" alt="Image" class="img-fluid mr-3">
              <div>
                <h3>Allison Holmes</h3>
                <span>Designer</span>
              </div>
            </div>
            <div>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Neque, mollitia. Possimus mollitia nobis libero quidem aut tempore dolore iure maiores, perferendis, provident numquam illum nisi amet necessitatibus. A, provident aperiam!</p>
            </div>
          </div>

        </div>
        
      </div>
    </div> -->


<!-- <div class="section-bg style-1" style="background-image: url('<?= $baseUrl ?>/images/hero_1.jpg');">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
            <span class="icon flaticon-mortarboard"></span>
            <h3>Our Philosphy</h3>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis recusandae, iure repellat quis delectus ea? Dolore, amet reprehenderit.</p>
          </div>
          <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
            <span class="icon flaticon-school-material"></span>
            <h3>Academics Principle</h3>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis recusandae, iure repellat quis delectus ea?
              Dolore, amet reprehenderit.</p>
          </div>
          <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
            <span class="icon flaticon-library"></span>
            <h3>Key of Success</h3>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis recusandae, iure repellat quis delectus ea?
              Dolore, amet reprehenderit.</p>
          </div>
        </div>
      </div>
    </div> -->

<!-- <div class="news-updates">
  <div class="container">

    <div class="row">
      <div class="col-lg-9">
        <div class="section-heading">
          <h2 class="text-black">News &amp; Updates</h2>
          <a href="#">Read All News</a>
        </div>
        <div class="row">
          <div class="col-lg-6">
            <div class="post-entry-big">
              <a href="news" class="img-link"><img src="<?= $baseUrl ?>/images/blog_large_1.jpg" alt="Image" class="img-fluid"></a>
              <div class="post-content">
                <div class="post-meta">
                  <a href="#">November 6, 2020</a>
                  <span class="mx-1">/</span>
                  <a href="#">Admission</a>, <a href="#">Updates</a>
                </div>
                <h3 class="post-heading"><a href="news">Campus Camping and Learning Session</a></h3>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="post-entry-big horizontal d-flex mb-4">
              <a href="news" class="img-link mr-4"><img src="<?= $baseUrl ?>/images/blog_1.jpg" alt="Image" class="img-fluid"></a>
              <div class="post-content">
                <div class="post-meta">
                  <a href="#">October 26, 2020</a>
                  <span class="mx-1">/</span>
                  <a href="#">Admission</a>, <a href="#">Updates</a>
                </div>
                <h3 class="post-heading"><a href="news">Campus Camping and Learning Session</a></h3>
              </div>
            </div>

            <div class="post-entry-big horizontal d-flex mb-4">
              <a href="news" class="img-link mr-4"><img src="<?= $baseUrl ?>/images/blog_2.jpg" alt="Image" class="img-fluid"></a>
              <div class="post-content">
                <div class="post-meta">
                  <a href="#">October 16, 2020</a>
                  <span class="mx-1">/</span>
                  <a href="#">Admission</a>, <a href="#">Updates</a>
                </div>
                <h3 class="post-heading"><a href="news">Campus Camping and Learning Session</a></h3>
              </div>
            </div>

            <div class="post-entry-big horizontal d-flex mb-4">
              <a href="news" class="img-link mr-4"><img src="<?= $baseUrl ?>/images/blog_1.jpg" alt="Image" class="img-fluid"></a>
              <div class="post-content">
                <div class="post-meta">
                  <a href="#">October 6, 2020</a>
                  <span class="mx-1">/</span>
                  <a href="#">Admission</a>, <a href="#">Updates</a>
                </div>
                <h3 class="post-heading"><a href="news">Campus Camping and Learning Session</a></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="section-heading">
          <h2 class="text-black">Course Videos</h2>
          <a href="#">View All Videos</a>
        </div>
        <iframe src="https://www.youtube.com/embed/qlHCVs69biE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <iframe src="https://www.youtube.com/embed/YbEZOT8rVW4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div> -->

<div class="site-section ftco-subscribe-1" style="background-image: url('<?= $baseUrl ?>/images/bg_1.jpg')">
  <!-- <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-7">
        <h2>Subscribe to us!</h2>
        <p>You can have the best deal from us first when we have promo,</p>
        <div style="text-align:center;" id="error">
        </div>
      </div>
      <div class="col-lg-5">
        <form class="d-flex" id="subscribe_form">
          <input type="text" id="subs_email" name="subs_email" class="rounded form-control mr-2 py-3" placeholder="Enter your email">
          <button name="btn-subs" id="btn-subs" class="btn btn-primary rounded py-3 px-4" type="submit">Subscribe</button>
        </form>
      </div>
    </div>
  </div> -->
</div>
<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "course_session".
 *
 * @property int $cs_id
 * @property int|null $cs_course_id
 * @property string|null $cs_remark
 * @property string|null $cs_teacher
 * @property int|null $cs_teacher_id
 * @property string|null $cs_date_start
 * @property string|null $cs_date_end
 * @property string|null $cs_hour_start
 * @property string|null $cs_hour_end
 * @property string|null $cs_dateline
 * @property string|null $cs_email
 * @property string|null $cs_next_course
 * @property string|null $cs_price
 * @property string|null $cs_code
 * @property string|null $cs_doc
 * @property string|null $cs_desc
 * @property int|null $cs_created_by
 * @property string $cs_created_at
 * @property int|null $cs_modified_by
 * @property string $cs_modified_at
 */
class CourseSession extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_session';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cs_course_id', 'cs_teacher_id', 'cs_created_by', 'cs_modified_by'], 'integer'],
            [['cs_date_start', 'cs_date_end', 'cs_hour_start', 'cs_hour_end', 'cs_dateline', 'cs_created_at', 'cs_modified_at'], 'safe'],
            [['cs_desc'], 'string'],
            [['cs_remark', 'cs_teacher'], 'string', 'max' => 100],
            [['cs_email', 'cs_next_course', 'cs_code'], 'string', 'max' => 50],
            [['cs_price'], 'string', 'max' => 25],
            [['cs_doc'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cs_id' => 'Cs ID',
            'cs_course_id' => 'Cs Course ID',
            'cs_remark' => 'Cs Remark',
            'cs_teacher' => 'Cs Teacher',
            'cs_teacher_id' => 'Cs Teacher ID',
            'cs_date_start' => 'Cs Date Start',
            'cs_date_end' => 'Cs Date End',
            'cs_hour_start' => 'Cs Hour Start',
            'cs_hour_end' => 'Cs Hour End',
            'cs_dateline' => 'Cs Dateline',
            'cs_email' => 'Cs Email',
            'cs_next_course' => 'Cs Next Course',
            'cs_price' => 'Cs Price',
            'cs_code' => 'Cs Code',
            'cs_doc' => 'Cs Doc',
            'cs_desc' => 'Cs Desc',
            'cs_created_by' => 'Cs Created By',
            'cs_created_at' => 'Cs Created At',
            'cs_modified_by' => 'Cs Modified By',
            'cs_modified_at' => 'Cs Modified At',
        ];
    }
}
